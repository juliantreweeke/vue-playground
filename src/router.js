import Vue from "vue";
import VueRouter from "vue-router";
import Company from "./views/Company/company.vue";
import Search from "./views/Search/search.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Search",
    component: Search,
  },
  {
    path: "/company/:name",
    name: "Company submissions",
    component: Company,
  },
];

const router = new VueRouter({
  mode: "history",
  routes,
});

export default router;
