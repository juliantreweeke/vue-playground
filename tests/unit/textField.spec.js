import { shallowMount } from "@vue/test-utils";
import TextField from "../../src/components/TextField/textField.vue";

describe("TextField", () => {
  it("should emit change event when typing in input", async () => {
    const wrapper = shallowMount(TextField);
    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue("some value");

    expect(wrapper.find('input[type="text"]').element.value).toBe("some value");
    expect(wrapper.emitted()).toHaveProperty("change");
  });

  it("should not emit change event if minCharacters prop is set and input value is less then minCharacters value", async () => {
    const wrapper = shallowMount(TextField, {
      propsData: {
        minCharacters: 2,
      },
    });

    const textInput = wrapper.find('input[type="text"]');
    await textInput.setValue("s");

    expect(wrapper.emitted()).not.toHaveProperty("change");
  });
});
